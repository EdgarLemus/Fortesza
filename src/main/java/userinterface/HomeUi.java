package userinterface;

import net.serenitybdd.screenplay.targets.Target;

public class HomeUi {

	public static final Target BTN_DESPLEGAR_MENU = Target.the("").locatedBy("//button[@class='mat-menu-trigger dropbtn']");
	public static final Target BTN_FACTORING = Target.the("").locatedBy("//button[contains(text(),'Factoring')]");
	public static final Target BTN_OPCION_PERFIL = Target.the("").locatedBy("//mat-icon[text()='more_vert']");
	public static final Target BTN_SEGURIDAD = Target.the("").locatedBy("//span[text()='Seguridad']//ancestor::button");
	public static final Target TXT_CONTRASENA_ACTUAL = Target.the("").locatedBy("//mat-label[text()='Contraseña actual']//ancestor::span//preceding-sibling::input");
	public static final Target TXT_NUEVA_CONTRASENA = Target.the("").locatedBy("//mat-label[text()='Nueva contraseña']//ancestor::span//preceding-sibling::input");
	public static final Target TXT_CONFIRMAR_NUEVA_CONTRASENA = Target.the("").locatedBy("//mat-label[text()='Confirmar contraseña']//ancestor::span//preceding-sibling::input");
	public static final Target BTN_CAMBIAR_CONTRASENA = Target.the("").locatedBy("//span[contains(text(),'Cambiar contraseña')]//ancestor::button");
	public static final Target TXT_VALIDACION_CAMBIO_CONTRASENA = Target.the("").locatedBy("//div[contains(text()='Actualización correcta')]");
	public static final Target BTN_PRIMER_INVERSION = Target.the("").locatedBy("//span[contains(text(),'Realizar')]//ancestor::a");
	public static final Target BTN_CERRAR_SESION = Target.the("").locatedBy("//button[contains(text(),'Cerrar')]");
	public static final Target BTN_INICIAR_SESION = Target.the("").locatedBy("//button[@id='btn-sesion']");
}
