package tasks;

import static org.openqa.selenium.Keys.ESCAPE;

import interactions.EsperaExplicitamenteEnPantalla;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.actions.JavaScriptClick;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.questions.WebElementQuestion;
import net.serenitybdd.screenplay.waits.Wait;
import userinterface.HomeUi;
import utils.Utilidades;

public class EditarContrasena implements Task{

	private String nuevaContrasena = Utilidades.generatePassword();
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
				Wait.until(WebElementQuestion.the(HomeUi.BTN_DESPLEGAR_MENU), WebElementStateMatchers.isVisible()).forNoLongerThan(60).seconds(),
				Hit.the(ESCAPE).into(HomeUi.BTN_DESPLEGAR_MENU),
				EsperaExplicitamenteEnPantalla.estosSegundos(5),
				JavaScriptClick.on(HomeUi.BTN_DESPLEGAR_MENU),
				JavaScriptClick.on(HomeUi.BTN_FACTORING),
				Wait.until(WebElementQuestion.the(HomeUi.BTN_OPCION_PERFIL), WebElementStateMatchers.isVisible()).forNoLongerThan(60).seconds(),
				JavaScriptClick.on(HomeUi.BTN_OPCION_PERFIL),
				JavaScriptClick.on(HomeUi.BTN_SEGURIDAD),
				Enter.theValue(Utilidades.obtenerContrasena()).into(HomeUi.TXT_CONTRASENA_ACTUAL),
				Enter.theValue(nuevaContrasena).into(HomeUi.TXT_NUEVA_CONTRASENA),
				Enter.theValue(nuevaContrasena).into(HomeUi.TXT_CONFIRMAR_NUEVA_CONTRASENA),
				JavaScriptClick.on(HomeUi.BTN_CAMBIAR_CONTRASENA),
				Wait.until(WebElementQuestion.the(HomeUi.TXT_VALIDACION_CAMBIO_CONTRASENA), WebElementStateMatchers.isVisible()).forNoLongerThan(60).seconds());
		Utilidades.modificarValorProperties(nuevaContrasena);
	}

	public static EditarContrasena enLaPagina() {
		return Instrumented.instanceOf(EditarContrasena.class).withProperties();
	}
}
