package tasks;

import static org.openqa.selenium.Keys.ESCAPE;

import interactions.EsperaExplicitamenteEnPantalla;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.actions.JavaScriptClick;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.questions.WebElementQuestion;
import net.serenitybdd.screenplay.waits.Wait;
import userinterface.HomeUi;

public class CerrarSesion implements Task{

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Wait.until(WebElementQuestion.the(HomeUi.BTN_DESPLEGAR_MENU), WebElementStateMatchers.isVisible()).forNoLongerThan(60).seconds(),
				Hit.the(ESCAPE).into(HomeUi.BTN_DESPLEGAR_MENU),
				EsperaExplicitamenteEnPantalla.estosSegundos(5),
				JavaScriptClick.on(HomeUi.BTN_DESPLEGAR_MENU),
				JavaScriptClick.on(HomeUi.BTN_CERRAR_SESION));
	}

	public static CerrarSesion enLaPagina() {
		return Instrumented.instanceOf(CerrarSesion.class).withProperties();
	}
}
