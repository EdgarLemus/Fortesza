package interactions;

import java.util.Timer;
import java.util.TimerTask;

import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;

public class EsperaExplicitamenteEnPantalla implements Interaction{

	private int segundos;
	
	public EsperaExplicitamenteEnPantalla(int segundos) {
		this.segundos = segundos;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		Timer temporizador = new Timer();        
        TimerTask tarea = new TimerTask() {
            @Override
            public void run() {
           }
        };        
        temporizador.schedule(tarea, (segundos*1000));
	}

	public static EsperaExplicitamenteEnPantalla estosSegundos(int segundos) {
		return Instrumented.instanceOf(EsperaExplicitamenteEnPantalla.class).withProperties(segundos);
	}
}
