package stepdefinitions;

import java.io.IOException;

import cucumber.api.java.Before;
import cucumber.api.java.en.*;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.questions.WebElementQuestion;
import tasks.CerrarSesion;
import userinterface.HomeUi;

public class CerrarSesionStepDefinitions {

	@Before
	public void before() throws IOException {
		OnStage.setTheStage(new OnlineCast());
	}
	
	@When("^el usuario cierre sesion$")
	public void elUsuarioCierreSesion() {
	    OnStage.theActorInTheSpotlight().attemptsTo(CerrarSesion.enLaPagina());
	}


	@Then("^podra ver el boton de registrar en pantalla$")
	public void podraVerElBotonDeRegistrarEnPantalla() {
	    OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(WebElementQuestion.the(HomeUi.BTN_INICIAR_SESION), WebElementStateMatchers.isVisible()));
	}
}
