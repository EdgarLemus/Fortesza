package stepdefinitions;

import java.io.IOException;

import cucumber.api.java.*;
import cucumber.api.java.en.*;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.questions.WebElementQuestion;
import tasks.EditarContrasena;
import tasks.IniciarSesion;
import userinterface.HomeUi;
import utils.WebDriverFactory;

public class EditarPerfilUsuarioStepDefinitions {

	@Before
	public void before() throws IOException {
		OnStage.setTheStage(new OnlineCast());
	}
	
	@Given("^que un usuario se encuentra en la pagina$")
	public void queUnUsuarioSeEncuentraEnLaPagina() {
		OnStage.theActorCalled("").can(BrowseTheWeb.with(WebDriverFactory.goToWeb("https://staging.fortesza.com/login"))); 
	}

	@When("^realiza el inicio de sesion$")
	public void realizaElInicioDeSesion() {
	    OnStage.theActorInTheSpotlight().attemptsTo(IniciarSesion.enLaPagina());
	}

	@When("^realiza la actualizacion de su contrasena$")
	public void realizaLaActualizaciNDeSuInformacionDePerfil() {
	    OnStage.theActorInTheSpotlight().attemptsTo(EditarContrasena.enLaPagina());
	}

	@Then("^se muestra un mensaje indicando que la actualizacion de la contrasena$")
	public void seActualizaLaInformaciNDelPerfilDelUsuario() {
	    OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(WebElementQuestion.the(HomeUi.TXT_VALIDACION_CAMBIO_CONTRASENA), WebElementStateMatchers.isVisible()));
	}
}
